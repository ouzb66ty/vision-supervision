const jsonpack = require('jsonpack/main');
const PDFDocument = require('pdfkit');
const fs = require('fs');
const redis = require('redis');
const ObjectID = require('mongodb').ObjectID;

module.exports.API = class API {

    static deleteAssoc(db, req, res, sequencer, user) {

        var request = {};
        var deleteFolderRecursive = function(path) {
            if ( fs.existsSync(path) ) {
                fs.readdirSync(path).forEach(function(file,index){
                    var curPath = path + "/" + file;
                    if(fs.lstatSync(curPath).isDirectory()) {
                        deleteFolderRecursive(curPath);
                    } else {
                        fs.unlinkSync(curPath);
                    }
                });
                fs.rmdirSync(path);
            }
        };

        if (user.payload.is_admin == true) {
            request = {'jsonRes._id': req.params.id};
        } else {
            request = {
                'jsonRes._id': req.params.id,
                'jsonRes.username': user.payload.username
            }
        }

        db.collection('active_cameras').findOne(request, (err, active_camera) => {
            if (err) throw err;

            if (active_camera) {
                db.collection('areas').deleteMany({'assoc': req.params.id});
                deleteFolderRecursive('public/sequences/' + active_camera.jsonRes.pathSequence);
                db.collection('active_cameras').deleteOne({'jsonRes._id': req.params.id});
                db.collection('detections').deleteMany({'assoc': req.params.id});
                sequencer.remove(req.params.id);
                res.json({'success': 'All associate datas from cameras deleted'});
            }
        });

    }

    /**
     * @api {get} /vision/v1/active_videos Get all active videos
     * @apiName getActiveVideos
     * @apiGroup active_videos
     * @apiVersion 1.0.0
     *
     * @apiSuccess {String} videos All active videos
     *
     * @apiParam {Number} n Active videos number to give.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           "active_videos": [{
     *               "_id": "5c9295e5f0fa6942e3297c1b",
     *               "pathname": "/public/videos/brigade.webm",
     *               "namespace": "brigade"
     *           }]
     *      }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     *
     */

    static getActiveCameras(db, req, res, sequencer, user) {

        var request = {};

        if (user.payload.is_admin == true) request = {};
        else request = { 'jsonRes.username': user.payload.username }

        db.collection('active_cameras').find(request).toArray((err, active_cameras) => {
            if (err) {
                res.statusCode = 500;
                res.json({ 'error' : 'Internal Server Error' });
                throw err;
            } else {
                res.json({ active_cameras });
            }
        });

    }

    static getCamera(db, req, res, sequencer, user) {

        const request = require('request');
        var requestDB = {};

        if (user.payload.is_admin == true) requestDB = {'jsonRes._id': req.params.id};
        else requestDB = { 'jsonRes.username': user.payload.username, 'jsonRes._id': req.params.id };

        db.collection('active_cameras').findOne(requestDB, (err, camera) => {
            if (err) {
                res.statusCode = 500;
                res.json({ 'error' : 'Internal Server Error' });
                throw err;
            } else {

                if (camera) {

                    camera = camera.jsonRes;
                    res.json(camera);

                } else {

                    request({

                        url: 'http://127.0.0.1:8001/orizon/v1/cameras/id/' + req.params.id,
                        method: 'GET',
                        headers: {
            'Authorization': 'Bearer ' + req.token,
            'Content-Type': 'application/json'
                        }

                    }, (err, resp, body) => {
                        var camera = JSON.parse(body);

                        res.json(camera);
                    });

                }
            }

        });

    }

    /**
     * @api {post} /vision/v1/alarm/{name} Add new area alarm
     * @apiName postAlarm
     * @apiGroup alarm
     * @apiVersion 1.0.0
     *
     * @apiSuccess {String} success Successfully add new area alarm
     *
     * @apiParam {Number} name Area name.
     * @apiParam {Number} categories Categories to alert.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'alarmArea': {}
     *     }
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static postAlarm(db, req, res, sequencer, user) {

        var redisClient = redis.createClient();
        const request = require('request');
        var timestampFrom = parseInt(req.body.timestampFrom);
        var timestampTo = parseInt(req.body.timestampTo);

        db.collection('areas').findOneAndUpdate(
            { "name": req.params.name },
            { $set: { 'isAlarm': true, 'fromValue': req.body.fromValue, 'toValue': req.body.toValue, 'categories': req.body.categories.split(','), 'timestampFrom': timestampFrom, 'timestampTo': timestampTo, 'username': user.payload.username } },
            { returnOriginal: false },
            function (err, alarmArea) {

                if (err) {

                    res.statusCode = 500;
                    res.json({'error': 'Internal Server Error'});

                } else {

                    redisClient.hset('visiolab-vision-' + alarmArea.value.assoc + '-alarms', alarmArea.value.name, JSON.stringify(alarmArea.value));
                    res.json({ 'alarmArea': alarmArea.value });

                }

            }
        );

    }

    /**
     * @api {get} /vision/v1/alarm/{name} get area alarm by name
     * @apiName getAlarm
     * @apiGroup alarm
     * @apiVersion 1.0.0
     *
     * @apiSuccess {String} alarmArea
     *
     * @apiParam {Number} name Area name.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'alarmArea': {}
     *     }
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static getAlarm(db, req, res, sequencer, user) {

        var redisClient = redis.createClient();
        var request = {};

        if (user.payload.is_admin == true) {
            request = { "name": req.params.name };
        } else {
            request = { 'username': user.payload.username, "name": req.params.name };
        }

        db.collection('areas').findOne(
            request,
            function (err, alarmArea) {

                if (err) {

                    res.statusCode = 500;
                    res.json({'error': 'Internal Server Error'});

                } else {

                    res.json({ alarmArea });

                }

            }
        );

    }

    static getAlarms(db, req, res, sequencer, user) {

        var request = {};

        if (user.payload.is_admin == true) {
            request = {};
        } else {
            request = { 'username': user.payload.username };
        }

        db.collection('areas').find(request).toArray((err, alarms) => {

            if (err) {

                res.statusCode = 500;
                res.json({'error': 'Internal Server Error'});

            } else {

                res.json({ alarms });

            }

        });

    }

    /**
     * @api {post} /vision/v1/active_videos Add new video in detection mode
     * @apiName postActiveVideos
     * @apiGroup active_videos
     * @apiVersion 1.0.0
     *
     * @apiSuccess {String} success Successfully add new active video (detection on)
     *
     * @apiParam {Number} id Video Users unique ID.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'new'
     *     }
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success' : 'pause'
     *     }
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success' : 'resume'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 502 Bad Gateway ou Proxy Error
     *     {
     *          'error': "Can't obtain Video ID from Orizon API"
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static postActiveCameras(db, req, res, sequencer, user) {

        const request = require('request');
        let msg = {};

        request({

            url: 'http://127.0.0.1:8001/orizon/v1/cameras/id/' + req.body.id,
            method: 'GET',
            headers: {
'Authorization': 'Bearer ' + req.token,
'Content-Type': 'application/json'
            }

        }, (err, resp, body) => {

            if (err) {
                res.statusCode = 500;
                res.json({ 'error' : 'Internal Server Error' });
                throw err;

            } else {

                const jsonRes = JSON.parse(body);

                jsonRes.detected = false;
                jsonRes.isActive = true;
                if (resp.statusCode === 404 ) {

                    res.statusCode = 502;
                    res.json( { 'error' : "Can't obtain Camera ID from Orizon API" } );

                } else {

                    /* Camera detection already ON verification */
                    db.collection('active_cameras').findOne({ 'jsonRes._id' : req.body.id },
                        (err, active_camera) => {

                            if (active_camera) {

                                if (sequencer.isActive(req.body.id) === true) {
                                    sequencer.pause(req.body.id);
                                    db.collection("active_cameras").updateOne({"jsonRes._id": req.body.id}, {$set: {"jsonRes.isActive": false}});
                                    res.json({ 'success' : 'pause' });
                                } else {
                                    sequencer.resume(req.body.id);
                                    db.collection("active_cameras").updateOne({"jsonRes._id": req.body.id}, {$set: {"jsonRes.isActive": true}});
                                    res.json({ 'success' : 'resume' });
                                }

                            } else {

                                /* Insert new camera detection ON */
                                db.collection('active_cameras').insertOne({jsonRes}, (err, dbRes) => {

                                    if (err) {

                                        res.statusCode = 500;
                                        res.json({ 'error' : 'Camera already active' });
                                        throw err;

                                    } else {

                                        /* Sequence cameras & detect */
                                        sequencer.add(jsonRes, 0);
                                        res.json( { 'success' : "new" } );

                                    }

                                });
                            }

                        });

                }

            }

        });

    }

    /**
     * @api {get} /vision/v1/areas/{id} Delete one area
     * @apiName deleteArea
     * @apiGroup areas
     * @apiVersion 1.0.0
     *
     * @apiSuccess {object} areas {}
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           "areas": []
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     *
     */

    static deleteArea(db, req, res, user) {

        db.collection('areas').deleteOne(
            {
                'name': req.body.name,
                'coords.0': req.body.x,
                'coords.1': req.body.y,
                'coords.2': req.body.w,
                'coords.3': req.body.h
            },
            (err, dbResult) => {
                if (err) throw err;

                res.json(dbResult);
            });

    }

    /**
     * @api {get} /vision/v1/areas/{id} Get all areas
     * @apiName getVideoAreas
     * @apiGroup areas
     * @apiVersion 1.0.0
     *
     * @apiSuccess {object} areas {}
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           "areas": []
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     *
     */

    static getCameraAreas(db, req, res, sequencer, user) {

        db.collection('areas').find({'assoc': req.params.id}).toArray(

            (err, areas) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    res.json({areas});

                }

            });

    }

    /**
     * @api {get} /vision/v1/alarms Get all alarms detections from active_cameras sources
     * @apiName getDetectionsAlarms
     * @apiGroup detections
     * @apiVersion 1.0.0
     *
     * @apiSuccess {object} detections {}
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           "detections": [
     *               {
     *                   "_id": "5c9cee3cfff3b55c358551af",
     *                   "filename": "../public/sequences/Plage/169c5026fef.jpg",
     *                   "res": 1,
     *                   "dets": [],
     *                   "join": "5c917da654eb9e77915e23e2"
     *               },
     *               {
     *                   "_id": "5c9cee3cfff3b55c358551b0",
     *                   "filename": "../public/sequences/Plage/169c5026a23.jpg",
     *                   "res": 1,
     *                   "dets": [],
     *                   "join": "5c917da654eb9e77915e23e2"
     *                }
     *          ]
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     *
     */

    static getDetectionsAlarms(db, req, res, sequencer, user) {

        const redis = require('redis');
        const client = redis.createClient();
        let saveJSON;
        let arrayJSON = [];
        let request = {};

        if (user.payload.is_admin == true) request = {};
        else request = { 'username' : user.payload.username };

        db.collection('areas').find(request).project({'name': 1, '_id': 1}).toArray((err, alarms) => {

            if (alarms) {

                var alarmsTmp = [];
                var pass = false;

                for (var i = 0; i < alarms.length; i++) {
                    alarmsTmp.push(alarms[i].name);
                }
                db.collection('detections').find({
                    'alarms': { $gte : 1, },
                    'alarms': { $elemMatch: { $elemMatch: { $in: alarmsTmp } } } }).sort({'_id': -1}).toArray((err, detections) => {
                    if (err) {
                        res.statusCode = 500;
                        res.json({ 'error' : 'Internal Server Error' });
                        throw err;
                    } else {
                        client.lrange('visiolab-vision-detections', 0, sequencer.dumpSize, (err, replies) => {

                            if (err) throw err;

                            for (let i = 0; i < replies.length; i++) {
                                saveJSON = jsonpack.unpack(replies[i].substr(1, replies[i].length - 2));
                                pass = false;
                                if (saveJSON.alarms && saveJSON.alarms.length > 0) {
                                    for (var i2 = 0; i2 < saveJSON.alarms.length; i2++) {
                                        for (var i3 = 0; i3 < alarmsTmp.length; i3++) {
                                            if (saveJSON.alarms[i2][0] == alarmsTmp[i3]) {
                                                arrayJSON.unshift(saveJSON);
                                                pass = true;
                                                break
                                            }
                                        }
                                        if (pass == true) break
                                    }
                                }
                            }
                            detections = arrayJSON.concat(detections);
                            res.json({detections});
                        });
                    }
                });

            } else {

                res.json({ 'error' : 'Not found' });

            }

        });

    }

    /**
     * @api {get} /vision/v1/detections/videos/{id} Get all detections from active videos ID
     * @apiName getVideoDetections
     * @apiGroup detections
     * @apiVersion 1.0.0
     *
     * @apiSuccess {object} detections {}
     *
     * @apiParam {id} id Associate id video.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           "detections": [
     *               {
     *                   "_id": "5c9cee3cfff3b55c358551af",
     *                   "filename": "../public/sequences/Plage/169c5026fef.jpg",
     *                   "res": 1,
     *                   "dets": [],
     *                   "join": "5c917da654eb9e77915e23e2"
     *               },
     *               {
     *                   "_id": "5c9cee3cfff3b55c358551b0",
     *                   "filename": "../public/sequences/Plage/169c5026a23.jpg",
     *                   "res": 1,
     *                   "dets": [],
     *                   "join": "5c917da654eb9e77915e23e2"
     *                }
     *          ]
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     *
     */

    static getCameraDetections(db, req, res, sequencer, user) {

        let saveJSON = {};
        let arrayJSON = [];
        const redis = require('redis');
        const client = redis.createClient();

        db.collection('detections').find({'assoc': req.params.id}).toArray(

            (err, detections) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    client.lrange('visiolab-vision-detections', 0, sequencer.dumpSize, (err, replies) => {
                        if (replies) {
                            for (let i = 0; i < replies.length; i++) {
                                saveJSON = jsonpack.unpack(replies[i].substr(1, replies[i].length - 2));
                                if (saveJSON.assoc == req.params.id)
                                    arrayJSON.push(saveJSON);
                            }
                        }
                        detections = detections.concat(arrayJSON);
                        res.json({detections});
                    });

                }

            });
    }

    static getOneDetection(db, req, res, sequencer, user) {

        let saveJSON = {};
        let arrayJSON = [];
        const redis = require('redis');
        const client = redis.createClient();
        var detection = null;

        db.collection('detections').find({'assoc': req.params.id}).toArray(

            (err, detections) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    client.lrange('visiolab-vision-detections', 0, sequencer.dumpSize, (err, replies) => {
                        if (replies) {
                            for (let i = 0; i < replies.length; i++) {
                                saveJSON = jsonpack.unpack(replies[i].substr(1, replies[i].length - 2));
                                if (saveJSON.assoc == req.params.id)
                                    arrayJSON.push(saveJSON);
                            }
                        }
                        detections = detections.concat(arrayJSON);
                        for (var i = 0; i < detections.length; i++) {
                            if (detections[i].progressSec == parseInt(req.params.progressSec)) {
                                detection = detections[i];
                            }
                        }
                        res.json({detection});
                    });

                }

            });
    }

    static postExport(db, req, res, sequencer, user) {

        let saveJSON = {};
        let arrayJSON = [];
        const redis = require('redis');
        const client = redis.createClient();
        var detection = null;
        var Pdfkit = require('pdfkit');
        var fs = require('fs');

        db.collection('detections').find({'assoc': req.params.id}).toArray(

            (err, detections) => {

                if (err) {

                    res.statusCode = 500;
                    res.json({
                        'error': 'Internal Server Error'
                    });
                    throw err;

                } else {

                    new Promise((resolve, reject) => {

                        /* Associate Redis Dump to MongoDB datas */
                        client.lrange('visiolab-vision-detections', 0, sequencer.dumpSize, (err, replies) => {
                            if (replies) {
                                for (let i = 0; i < replies.length; i++) {
                                    saveJSON = jsonpack.unpack(replies[i].substr(1, replies[i].length - 2));
                                    if (saveJSON.assoc == req.params.id)
                                        arrayJSON.push(saveJSON);
                                }
                            }
                            detections = detections.concat(arrayJSON);
                            for (var i = 0; i < detections.length; i++) {
                                if (detections[i].progressSec == parseInt(req.body.progressSec)) {
                                    detection = detections[i];
                                }
                            }

                            resolve(detection);
                        });

                    }).then((detection) => {

                        /* Generate PDF */
                        var doc = new Pdfkit;
                        var ext = req.body.statsDataUrl.split(';')[0].match(/jpeg|png|gif/)[0];
                        var data = req.body.statsDataUrl.replace(/^data:image\/\w+;base64,/, "");
                        var buf = new Buffer.from(data, 'base64');

                        if (!fs.existsSync('public/rapports'))
                            fs.mkdir('public/rapports', { recursive: true }, (err) => { if (err) throw err; });
                        doc.pipe(fs.createWriteStream('public/rapports/rapport.pdf'));
                        doc.image(detection.filename.substr(3, detection.filename.length), 10, 127, { width: 592 });
                        fs.writeFile('/tmp/tmp-export-visiolab.' + ext, buf, (err) => {
                            if (err) throw err;

                            doc.image('/tmp/tmp-export-visiolab.png', 10, 522, { width: 492 });
                            db.collection('active_cameras').findOne({'jsonRes._id': req.params.id}, (err, active_camera) => {
                                doc.fontSize(10).text('Domaine: ' + active_camera.jsonRes.namespace, 25, 35);
                                doc.fontSize(10).text('Nom du fichier: ' + active_camera.jsonRes.name, 25, 47);
                                doc.fontSize(10).text('Type: ' + active_camera.jsonRes.mimetype, 25, 59);
                                doc.fontSize(10).text('Sequence: ' + detection.progressSec / 1000 + '/' + detection.secDuration / 1000 + 'sec', 25, 71);
                                doc.fontSize(10).text('Auteur de l\'analyse: ' + active_camera.jsonRes.username, 25, 83);
                                doc.fontSize(10).text('Vidéo téléchargé ' + active_camera.jsonRes.date, 25, 95);
                                doc.fontSize(8).text('Visiolab - Rapport de détection', 5, 5);
                                doc.end();
                                res.json({ 'url': 'http://127.0.0.1:8003/rapports/rapport.pdf' });
                            });
                        });

                    });

                }

            });

    }

    /**
     * @api {post} /vision/v1/areas Apply new areas filters
     * @apiName postAreas
     * @apiGroup detections
     * @apiVersion 1.0.0
     *
     * @apiSuccess {String} success Successfully add new areas filters.
     *
     * @apiParam {Object} areas Contains name and coords of all areas.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'Areas successfully added'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static postAreas(db, req, res, sequencer, user) {

        for (let i = 0; i < req.body.length; i++) {
            if (!req.body[i].name || !req.body[i].coords || req.body[i].coords.length != 4) {
                res.statusCode = 400;
                res.json({ 'error' : 'Bad Parameters' });
                return ;
            }
        }
        req.body.isAlarm = false;
        db.collection('areas').insertOne(req.body, (err, dbResult) => {
            if (err) {
                res.statusCode = 500;
                res.json({ 'error' : 'Internal Server Error' });
            } else {
                res.json({
                    'success' : 'Areas successfully added'
                });
            }
        }, { ordered: false },);

    }

    /**
     * @api {post} /vision/v1/detections Dump Redis detections in MongoDB
     * @apiName postDetections
     * @apiGroup detections
     * @apiVersion 1.0.0
     *
     * @apiSuccess {String} success Successfully add new active camera (detection on)
     *
     * @apiParam {Number} dumpSize Redis dump size to stock in MongoDB.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          'success': 'Dump successfully'
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static postDetections(db, req, res, sequencer, user) {

        const redis = require('redis');
        const client = redis.createClient();
        const jsonpack = require('jsonpack');
        const dumpSize = req.body.dumpSize;
        let saveJSON = {};
        let arrayJSON = [];

        client.lrange("dets", 0, sequencer.dumpSize, (err, replies) => {
            if (err) {
                res.statusCode = 500;
                res.json({ 'error' : 'Internal Server Error' });
                throw err;
            } else {
                for (let i = 0; i < replies.length; i++) {
                    saveJSON = jsonpack.unpack(replies[i].substr(1, replies[i].length - 2));
                    arrayJSON.push(saveJSON);
                }
                db.collection('detections').insertMany(arrayJSON, (err, dbResult) => {
                    if (err) {
                        res.statusCode = 500;
                        res.json({ 'error' : 'Internal Server Error' });
                        throw err;
                    } else {
                        res.json({
                            'success' : 'Dump successfully'
                        });
                    }
                });
            }
        });

    }

    /**
     * @api {post} /vision/v1/detections/videos/search Search detections
     * @apiName postSearchVideosDetections
     * @apiGroup detections
     * @apiVersion 1.0.0
     *
     * @apiSuccess {Object} detections Return all request detections (by colors and class)
     *
     * @apiParam {class} Array of suggested class
     * @apiParam {colors} Array of suggested colors
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *
     *     }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *     {
     *          'error': 'Internal Server Error'
     *     }
     */

    static postSearchCamerasDetections(db, req, res, sequencer, user) {

        const redis = require('redis');
        const client = redis.createClient();
        const jsonpack = require('jsonpack');
        var saveJSON = {};
        var arrayJSON = [];

        /* Get areas MongoDB datas */
        db.collection('areas').find( { "assoc" : req.params.id } ).toArray((err, areas) => {

            if (err) {

                res.statusCode = 500;
                res.json({ 'error' : 'Internal Server Error' });
                throw err;

            } else {

                var reqClasses;
                var reqColors;
                var fromDate;
                var toDate;

                fromDate = parseInt(req.query.from);
                toDate = parseInt(req.query.to);
2

                /* Filters options checkbox by classes and colors */
                if (typeof req.query.classes == 'string')
                    reqClasses = [parseInt(req.query.classes)];
                else if (req.query.classes != undefined)
                    reqClasses = req.query.classes.map(Number);
                else {
                    reqClasses = undefined;
                }

                if (typeof req.query.colors == 'string')
                    reqColors = [parseInt(req.query.colors)];
                else if (req.query.colors != undefined)
                    reqColors = req.query.colors.map(Number);
                else
                    reqColors = [0, 1, 2, 3, 4, 5, 6, 7, 8];

                /* MongoDB request */
                if (reqClasses != undefined) {

                    if (fromDate != NaN && toDate != NaN && fromDate < toDate) {
                        var request = {
                            "assoc": req.params.id,
                            $and: [
                                {
                                    "dets.class": {
                                        $in: reqClasses
                                    }
                                },
                                {
                                    "dets.color": {
                                        $in: reqColors
                                    }
                                },
                                {
                                    "date": {
                                        $gte: fromDate,
                                        $lte: toDate
                                    }
                                }
                            ]
                        };
                    } else {
                        var request = {
                            "assoc": req.params.id,
                            $and: [
                                {
                                    "dets.class": {
                                        $in: reqClasses
                                    }
                                },
                                {
                                    "dets.color": {
                                        $in: reqColors
                                    }
                                }
                            ]
                        };
                    }

                } else {

                    if (fromDate != NaN && toDate != NaN && fromDate < toDate) {

                        var request = {
                            "assoc": req.params.id,
                            $and: [
                                {
                                    "dets": {
                                        $size: 0
                                    }
                                },
                                {
                                    "date": {
                                        $gte: fromDate,
                                        $lte: toDate
                                    }
                                }
                            ]
                        };

                    } else {

                        var request = {
                            "assoc": req.params.id,
                            "dets": {
                                $size: 0
                            }
                        };

                    }

                }

                /* Get MongoDB datas */
                db.collection('detections').find(request).sort({ date: -1 }).toArray((err, detections) => {

                    if (err) {

                        res.statusCode = 500;
                        res.json({ 'error' : 'Internal Server Error' });
                        throw err;

                    } else {

                        /* Get Redis Datas */
                        client.lrange('visiolab-vision-detections', 0, sequencer.dumpSize, (err, replies) => {
                            if (replies) {
                                for (let i = 0; i < replies.length; i++) {
                                    saveJSON = jsonpack.unpack(replies[i].substr(1, replies[i].length - 2));
                                    if (saveJSON.assoc == req.params.id)
                                        arrayJSON.unshift(saveJSON);
                                }
                            }

                            /* Filters Redis datas */
                            let newArray = [];

                            for (var sequence in arrayJSON) {
                                var inside = false;

                                if (reqClasses != undefined) {
                                    for (var detection in arrayJSON[sequence].dets) {
                                        if (inside == false
                                            && reqClasses.includes(arrayJSON[sequence].dets[detection].class)
                                            && reqColors.includes(arrayJSON[sequence].dets[detection].color)) {
                                            newArray.push(arrayJSON[sequence]);
                                            inside = true;
                                        }
                                    }
                                } else {
                                    /* If no selected class get all empty analyses */
                                    if (arrayJSON[sequence].dets.length == 0)
                                        newArray.push(arrayJSON[sequence]);
                                }
                            }

                            /* Concat two Databases */
                            detections = newArray.concat(detections);

                            /* Verify areas */
                            let areasDetections = [];

                            if (req.query.areas && areas) {
                                for (var i = 0; i < detections.length; i++) {
                                    var passed = false;

                                    for (var det in detections[i].dets) {
                                        areas.forEach((area) => {
                                            if (passed == false
                                                && req.query.areas.includes(area.name)
                                                && reqClasses.includes(detections[i].dets[det].class)
                                                && ((detections[i].dets[det].x >= area.coords[0]
                                                    && detections[i].dets[det].x <= area.coords[0] + area.coords[2]
                                                    && detections[i].dets[det].y >= area.coords[1]
                                                    && detections[i].dets[det].y <= area.coords[1] + area.coords[3])
                                                    || (detections[i].dets[det].x + detections[i].dets[det].w >= area.coords[0]
                                                        && detections[i].dets[det].x + detections[i].dets[det].w <= area.coords[0] + area.coords[2]
                                                        && detections[i].dets[det].y >= area.coords[1]
                                                        && detections[i].dets[det].y <= area.coords[1] + area.coords[3])
                                                    || (detections[i].dets[det].x >= area.coords[0]
                                                        && detections[i].dets[det].x <= area.coords[0] + area.coords[2]
                                                        && detections[i].dets[det].y + detections[i].dets[det].h >= area.coords[1]
                                                        && detections[i].dets[det].y + detections[i].dets[det].h <= area.coords[1] + area.coords[3])
                                                    || (detections[i].dets[det].x + detections[i].dets[det].w >= area.coords[0]
                                                        && detections[i].dets[det].x + detections[i].dets[det].w <= area.coords[0] + area.coords[2]
                                                        && detections[i].dets[det].y + detections[i].dets[det].h >= area.coords[1]
                                                        && detections[i].dets[det].y + detections[i].dets[det].h <= area.coords[1] + area.coords[3]))) {

                                                areasDetections.push(detections[i]);
                                                passed = true;
                                            }
                                        });
                                    }
                                }
                                detections = areasDetections;

                                /* Pagination */
                                let page = 84 * (req.params.n);

                                if (page > detections.length) {
                                    res.statusCode = 409;
                                    res.json({
                                        'error': 'Page not found'
                                    });
                                } else {
                                    var pageDets = [];

                                    for (var i = 0; i < 84 && (page + i) < detections.length ; i++) {
                                        pageDets[i].push(detections[page + i]);
                                    }
                                    detections = pageDets;
                                    res.json({detections});
                                }

                            } else {

                                /* Pagination */
                                let page = 84 * (req.params.n);

                                if (page > detections.length) {
                                    res.statusCode = 409;
                                    res.json({
                                        'error': 'Page not found'
                                    });
                                } else {
                                    var pageDets = [];

                                    for (var i = 0; i < 84 && (page + i) < detections.length ; i++) {
                                        pageDets.push(detections[page + i]);
                                    }
                                    detections = pageDets;
                                    res.json({detections});
                                }

                            }
                        });

                    }

                });
            }

        });

    }

}
